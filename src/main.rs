extern crate firmata;
extern crate serial;

use firmata::*;
use serial::*;
use std::thread;

fn main() {
    let mut sp = serial::open("/dev/ttyACM0").unwrap();

    sp.reconfigure(&|settings| {
        settings.set_baud_rate(Baud57600).unwrap();
        settings.set_char_size(Bits8);
        settings.set_parity(ParityNone);
        settings.set_stop_bits(Stop1);
        settings.set_flow_control(FlowNone);
        Ok(())
    }).unwrap();

    let mut b = firmata::Board::new(Box::new(sp)).unwrap();

    let data_pin = 4;  // SER   #14
    let latch_pin = 5; // RCLK  #12
    let clock_pin = 6; // SRCLK #11

    let mut data = 0xf0;
    let mut serial_value = 0;

    b.set_pin_mode(data_pin, firmata::OUTPUT);
    b.set_pin_mode(latch_pin, firmata::OUTPUT);
    b.set_pin_mode(clock_pin, firmata::OUTPUT);


    loop {
    for i in 0..8 {
        //serial_value = data & 1;
        serial_value = 1;
        println!("{}", serial_value);
        b.digital_write(latch_pin, 0);
        b.digital_write(data_pin, serial_value);
        b.digital_write(clock_pin, 1);
        b.digital_write(clock_pin, 0);
        b.digital_write(latch_pin, 1);

        //data = data >> 1;
        thread::sleep_ms(500);
    }
    for i in 0..8 {
        //serial_value = data & 1;
        serial_value = 0;
        println!("{}", serial_value);
        b.digital_write(latch_pin, 0);
        b.digital_write(data_pin, serial_value);
        b.digital_write(clock_pin, 1);
        b.digital_write(clock_pin, 0);
        b.digital_write(latch_pin, 1);

        //data = data >> 1;
        thread::sleep_ms(500);
    }
    }

    println!("Done.");
}
